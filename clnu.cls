% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.% Information on how to create LaTeX packages


\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{clnu}[2022/02/08 v0.0 CV]


%----------------------------------------------------------------------------------------
%   Declaration of provided macros
%----------------------------------------------------------------------------------------
% Signature:    \makeheader{}
% Description:  Prints header containing title and contact information
% Arguments:    -
\providecommand{\makeheader}{}

% Signature:    \setheadercolor{color_name}
% Description:  Defines the background color of the header
% Arguments:    color_name -> Existing or previously defined color name
\providecommand{\setheadercolor}[1]{}

% Signature:    \settitlecolor{color_name}
% Description:  Defines the cover letter's title color (if not white, icons switch from inverted to themecolor)
% Arguments:    color_name -> Existing or previously defined color name
\providecommand{\settitlecolor}[1]{}

% Signature:    \PERSONALINFORMATION
% Description:  Macro for personal information
% Arguments:    -
\providecommand*{\firstname}{firstName}
\providecommand*{\lastname}{lastName}
\providecommand*{\street}{street}
\providecommand*{\city}{city}
\providecommand*{\zip}{12345}
\providecommand*{\country}{country}
\providecommand*{\mobile}{+49 000 0000 0000}
\providecommand*{\email}{x@y.z}
\providecommand*{\linkedin}{in}
\providecommand*{\github}{github}
\providecommand*{\codeberg}{codeberg}
\providecommand*{\jobtitle}{jobtitle}
\providecommand*{\affiliation}{affiliation}

% Signature:    \setPERSONALINFORMATION{value}
% Description:  Defines the corresponding personal information macro
% Arguments:    value -> Value being assigned to personal information macro
\providecommand*{\setfirstname}[1]{}
\providecommand*{\setlastname}[1]{}
\providecommand*{\setstreet}[1]{}
\providecommand*{\setcity}[1]{}
\providecommand*{\setzip}[1]{}
\providecommand*{\setcountry}[1]{}
\providecommand*{\setmobile}[1]{}
\providecommand*{\setemail}[1]{}
\providecommand*{\setlinkedin}[1]{}
\providecommand*{\setgithub}[1]{}
\providecommand*{\setcodeberg}[1]{}
\providecommand*{\setjobtitle}[1]{}
\providecommand*{\setaffiliation}[1]{}


%----------------------------------------------------------------------------------------
%   Option handling
%----------------------------------------------------------------------------------------
\RequirePackage{xkeyval}
\RequirePackage{ifthen}

% Fallback
\DeclareOptionX*{\ClassWarning{clnu}{Unknown option '\CurrentOption'}}

% Pass parent class options
\DeclareOptionX*{\PassOptionsToClass{\CurrentOption}{article}}

% Select header style
\newif\ifHeaderIsNotSet\HeaderIsNotSettrue
\DeclareOptionX{header}{%
    \ifthenelse{\equal{\detokenize{#1}}{\detokenize{basic}}}%
        {\input{macros/header/basic_cl.tex}\HeaderIsNotSetfalse}%
        {}%
    \ifthenelse{\equal{\detokenize{#1}}{\detokenize{topbar}}}%
        {\input{macros/header/topbar_cl.tex}\HeaderIsNotSetfalse}%
        {}%
}

% Use city, country instead of detailed address
\DeclareOptionX{international}{\def\international{}}

% Enable options
\ProcessOptionsX\relax

% Set standard options if keys have not been used
\ifHeaderIsNotSet\input{macros/header/basic_cl.tex}\fi

% Load base class
\LoadClass{article}


%----------------------------------------------------------------------------------------
%   Import packages
%----------------------------------------------------------------------------------------
\RequirePackage{calc}
\RequirePackage{fontspec} % Use of locally intalled fonts 
\RequirePackage{geometry} % Set page margins
\RequirePackage{xcolor} % Definition of custom colors via \definecolor
\RequirePackage[hidelinks]{hyperref} % For adding URL's to text
\RequirePackage{tikz} % For colored bar and timeline
\RequirePackage{etoolbox} % Required for 1 - 5 skill evaluation and comparison between colors (\ifdefequal{\@spec@A}{\@spec@B} in print_personal.tex)
\RequirePackage{eqparbox} % Replacement for minipage that sizes box automatically


%----------------------------------------------------------------------------------------
%   Definition of provided macros
%----------------------------------------------------------------------------------------
\input{macros/set_personal.tex}
\newcommand{\setcvsectioncolor}{} % Create dummy command to avoid issue including color set macros
\input{macros/set_color.tex}


%----------------------------------------------------------------------------------------
%   Definition of standard values
%----------------------------------------------------------------------------------------
\definecolor{darkblue}{RGB}{15, 43, 70}
\definecolor{blue}{RGB}{17, 75, 118}
\colorlet{headercolor}{darkblue}
\colorlet{titlecolor}{blue}
\title{Application} % Set standard tile
\setmainfont{Calibri} % Set font
\geometry{top=1.69cm, left=1.5cm, right=1.5cm, bottom=1.5cm} % Adapt page margins
\pagestyle{empty} % Disable page numbers
\setlength{\parindent}{0pt} % Disable indentation
\setlength{\parskip}{0cm} % Add space between paragraphs

%% EOF
\endinput