% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this
% file, You can obtain one at https://mozilla.org/MPL/2.0/.% Information on how to create LaTeX packages


% https://latexref.xyz/Class-and-package-construction.html
% https://en.wikibooks.org/wiki/LaTeX/Creating_Packages
% https://www.latex-project.org/help/documentation/clsguide.pdf
% https://de.overleaf.com/learn/latex/Writing_your_own_class
% https://www.tug.org/TUGboat/tb26-3/tb84heff.pdf
% https://github.com/jbezos/enumitem/blob/master/enumitem.sty
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{cvnu}[2022/02/08 v0.0 CV]


%----------------------------------------------------------------------------------------
%   Declaration of provided macros
%----------------------------------------------------------------------------------------
% Signature:    \makeheader{}
% Description:  Prints header containing title and contact information
% Arguments:    -
\providecommand{\makeheader}{}

% Signature:    \cvsection{title}
% Description:  Prints cv section title, e.g. education
% Arguments:    title -> Section title
\providecommand{\cvsection}[1]{}

% Signature:    \cvitem[timeline_continues]{title}{start_date}{end_date}{location}{detail}
% Description:  Prints cv item consisting, e.g. job
% Arguments:    title -> Job title
%               start_date -> Start date of occupation
%               end_date (optional) -> End date of occupation
%               location -> Company name incl. location
%               detail -> Highlights of job
%               location -> The company's or university's location
%               timeline_continues (optional, default:1) -> Add padding to cover white space to timeline
\providecommand{\cvitem}[7][1]{}

% Signature:    \setheadercolor{color_name}
% Description:  Defines the background color of the header
% Arguments:    color_name -> Existing or previously defined color name
\providecommand{\setheadercolor}[1]{}

% Signature:    \settitlecolor{color_name}
% Description:  Defines the cv title's color (if not white, icons switch from inverted to themecolor)
% Arguments:    color_name -> Existing or previously defined color name
\providecommand{\settitlecolor}[1]{}

% Signature:    \setcvsectioncolor{color_name}
% Description:  Defines the cv section's color
% Arguments:    color_name -> Existing or previously defined color name
\providecommand{\setcvsectioncolor}[1]{}

% Signature:    \skillrating{skill}{rating}
% Description:  Prints a skill rating using five dots
% Arguments:    skill -> Name of the skill to state and rate, e.g. Pyton
%               rating -> Integer between 1 and 5
\providecommand{\skillrating}[2]{}

% Signature:    \setcvsectionskip{length}
% Description:  Set length used inside vspace between cv sections
% Arguments:    length -> Value to be assigned
\providecommand{\setcvsectionskip}[1]{}

% Signature:    \setcvitemskip{length}
% Description:  Set length used inside vspace between cv items
% Arguments:    length -> Value to be assigned
\providecommand{\setcvitemskip}[1]{}

% Signature:    \setcvdetailskip{length}
% Description:  Set length used for parsep inside itemize environment for use in cvitem macro
% Arguments:    length -> Value to be assigned
\providecommand{\setcvdetailskip}[1]{}

% Signature:    \sethintscolumnwidth{length}
% Description:  Width of time period specifying cv items
% Arguments:    length -> Value to be assigned
\providecommand{\sethintscolumnwidth}[1]{}

% Signature:    \setmaincolumndwidth{length}
% Description:  Width of cv item
% Arguments:    length -> Value to be assigned
\providecommand{\setmaincolumnwidth}[1]{}

% Signature:    \PERSONALINFORMATION
% Description:  Macro for personal information
% Arguments:    -
\providecommand*{\firstname}{firstName}
\providecommand*{\lastname}{lastName}
\providecommand*{\street}{street}
\providecommand*{\city}{city}
\providecommand*{\zip}{12345}
\providecommand*{\country}{country}
\providecommand*{\mobile}{+49 000 0000 0000}
\providecommand*{\email}{x@y.z}
\providecommand*{\linkedin}{in}
\providecommand*{\github}{github}
\providecommand*{\codeberg}{codeberg}
\providecommand*{\jobtitle}{jobtitle}
\providecommand*{\affiliation}{affiliation}

% Signature:    \setPERSONALINFORMATION{value}
% Description:  Defines the corresponding personal information macro
% Arguments:    value -> Value being assigned to personal information macro
\providecommand*{\setfirstname}[1]{}
\providecommand*{\setlastname}[1]{}
\providecommand*{\setstreet}[1]{}
\providecommand*{\setcity}[1]{}
\providecommand*{\setzip}[1]{}
\providecommand*{\setcountry}[1]{}
\providecommand*{\setmobile}[1]{}
\providecommand*{\setemail}[1]{}
\providecommand*{\setlinkedin}[1]{}
\providecommand*{\setgithub}[1]{}
\providecommand*{\setcodeberg}[1]{}
\providecommand*{\setjobtitle}[1]{}
\providecommand*{\setaffiliation}[1]{}


%----------------------------------------------------------------------------------------
%   Option handling
%----------------------------------------------------------------------------------------
\RequirePackage{xkeyval}
\RequirePackage{ifthen}

% Fallback
\DeclareOptionX*{\ClassWarning{cvnu}{Unknown option '\CurrentOption'}}

% Pass parent class options
\DeclareOptionX*{\PassOptionsToClass{\CurrentOption}{article}}

% Select header style
\newif\ifHeaderIsNotSet\HeaderIsNotSettrue
\DeclareOptionX{header}{%
    \ifthenelse{\equal{\detokenize{#1}}{\detokenize{basic}}}%
        {\input{macros/header/basic.tex}\HeaderIsNotSetfalse}%
        {}%
    \ifthenelse{\equal{\detokenize{#1}}{\detokenize{topbar}}}%
        {\input{macros/header/topbar.tex}\HeaderIsNotSetfalse}%
        {}%
    \ifthenelse{\equal{\detokenize{#1}}{\detokenize{academic}}}%
        {\input{macros/header/academic.tex}\HeaderIsNotSetfalse}%
        {}%
}

% Select cv item style
\newif\ifItemStyleIsNotSet\ItemStyleIsNotSettrue
\DeclareOptionX{itemstyle}{
    \ifthenelse{\equal{\detokenize{#1}}{\detokenize{basic}}}
        {\input{macros/itemstyle/basic.tex}\ItemStyleIsNotSetfalse}%
        {}%
    \ifthenelse{\equal{\detokenize{#1}}{\detokenize{timeline}}}
        {\input{macros/itemstyle/timeline.tex}\ItemStyleIsNotSetfalse}%
        {}%
    \ifthenelse{\equal{\detokenize{#1}}{\detokenize{dotline}}}
        {\input{macros/itemstyle/dotline.tex}\ItemStyleIsNotSetfalse}%
        {}%
    \ifthenelse{\equal{\detokenize{#1}}{\detokenize{sidedash}}}
        {\input{macros/itemstyle/sidedash.tex}\ItemStyleIsNotSetfalse}%
        {}%
    \ifthenelse{\equal{\detokenize{#1}}{\detokenize{academic}}}
        {\input{macros/itemstyle/academic.tex}\ItemStyleIsNotSetfalse}%
        {}%
}

% Use city, country instead of detailed address
\DeclareOptionX{international}{\def\international{}}

% Enable options
\ProcessOptionsX\relax

% Set standard options if keys have not been used
\ifHeaderIsNotSet\input{macros/header/basic.tex}\fi
\ifItemStyleIsNotSet\input{macros/itemstyle/basic.tex}\fi

% Load base class
\LoadClass{article}


%----------------------------------------------------------------------------------------
%   Import packages
%----------------------------------------------------------------------------------------
\RequirePackage{calc}
\RequirePackage{fontspec} % Use of locally intalled fonts 
\RequirePackage{geometry} % Set page margins
\RequirePackage{xcolor} % Definition of custom colors via \definecolor
\RequirePackage[hidelinks]{hyperref} % For adding URL's to text
\RequirePackage{tikz} % For colored bar and timeline
\RequirePackage{etoolbox} % Required for 1 - 5 skill evaluation and comparison between colors (\ifdefequal{\@spec@A}{\@spec@B} in print_personal.tex)
\RequirePackage{eqparbox} % Replacement for minipage that sizes box automatically
\RequirePackage{array} % For defining own cell type, e.g. centered cell with defined width
\RequirePackage[inline]{enumitem} % Use commands such as \setlist to adapt itemize and enumerate environments


%----------------------------------------------------------------------------------------
%   Definition of provided macros
%----------------------------------------------------------------------------------------
\input{macros/set_personal.tex}
\input{macros/set_color.tex}
\input{macros/set_length.tex}
\input{macros/skill_rating.tex}
\newcolumntype{x}[1]{>{\centering\let\newline\\\arraybackslash\hspace{0pt}}p{#1}} % Define centered cell for tabular environment with defined width


%----------------------------------------------------------------------------------------
%   Definition of standard values
%----------------------------------------------------------------------------------------
\definecolor{darkblue}{RGB}{15, 43, 70}
\definecolor{blue}{RGB}{17, 75, 118}
\colorlet{headercolor}{darkblue}
\colorlet{titlecolor}{blue}
\colorlet{cvsectioncolor}{blue}
\newlength{\cvsectionskip}
\setcvsectionskip{1.5\baselineskip}
\newlength{\cvitemskip}
\setcvitemskip{6mm}
\newlength{\cvdetailskip}
\setcvdetailskip{0.8mm}
\newlength{\maincolumnwidth}
\setmaincolumnwidth{16.4cm}
\newlength{\hintscolumnwidth}
\sethintscolumnwidth{1.6cm}
\title{Curriculum Vitae} % Set standard title
\setmainfont{Calibri} % Set font
\geometry{top=1cm, left=1cm, right=1cm, bottom=1cm} % Adapt page margins
\pagestyle{empty} % Disable page numbers
\setlength{\parindent}{0pt} % Disable indentation
\setlist[itemize]{topsep=0mm, partopsep=0mm, itemsep=0mm, parsep=\cvdetailskip, leftmargin=5mm} % Define spacing for itemize environments
\renewcommand{\labelitemi}{$\circ$} % Change bullet point to empty circle

%% EOF
\endinput