cvnu
====

[![License: MPL 2.0](https://img.shields.io/badge/License-MPL_2.0-brightgreen.svg)](https://opensource.org/licenses/MPL-2.0)

This class provides macros to create resume entries and sections,
as well as a header to display the title and contact information.
Entry- and headerstyle can be changed by invoking the class with
different options.
The design focuses on emphasising the content and tries to use design
elements only for the benefit of clarity and/ or saving space.

header=basic, itemstyle=basic | header=basic, itemstyle=sidedash | header=academic, itemstyle=academic | header=topbar, itemstyle=timeline | header=topbar, itemstyle=dotline
:---:|:---:|:---:|:---:|:---:
![](./example/cv_basic.png "basic-basic") | ![](./example/cv_sidedash.png "basic-sidedash") | ![](./example/cv_academic.png "academic-academic") |![](./example/cv_timeline.png "topbar-timeline") | ![](./example/cv_dotline.png "topbar-dotline")


Usage
-----

| cvnu | clnu |
|---|---|
|\documentclass[11pt, a4paper, international, header=basic, itemstyle=basic]{cvnu}<ul><li> international </li><ul><li> Optional flag </li><li>Switch between detailed address or "city, country" only</li></ul><li>header</li><ul><li>basic (standard)</li><li>academic</li><li>topbar</li></ul><li>itemstyle</li><ul><li>basic (standard)</li><li>academic</li><li>timeline</li><li>sidedash</li><li>dotline</li></ul></ul> | \documentclass[12pt, a4paper, international, header=basic]{clnu}<ul><li> international </li><ul><li> Optional flag </li><li>Switch between detailed address or "city, country" only</li></ul><li>header</li><ul><li>basic (standard)</li></ul></ul>|

All provided macros are declared in the top of the class files 
cvnu.cls and clnu.cls (suitable cover letter design).
See example folder to get started.


License
-------

The package is subject to MPL-2.0, allowing commercial use, 
distribution, modification, patent use and private use. Upon sharing/ 
distribution, one has to disclose the source, include the license and copyright notice and keep the same license for modified files. Unlike
LGPL, changes do not have to be stated.

The icons provided alongside of the package are subject to
their own copyright:

| | |
|---|---|
| LinkedIn | https://brand.linkedin.com/policies |
| GitHub | https://github.com/logos |
| Codeberg | https://codeberg.org/Codeberg/Design/wiki/Branding |


Installation
------------

Add a local folder to your MiKTeX distribution, see 
https://tex.stackexchange.com/questions/69483/create-a-local-texmf-tree-in-miktex
for details.


Repository
----------

https://codeberg.org/codebergNu/cvnu.git